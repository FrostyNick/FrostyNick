### Hi there 👋

- 🌱 I’m currently learning about [Rust](https://doc.rust-lang.org/stable/book/) and SQL databases.
- 💬 Ask me about game development, Lua, Bash, Python, C++, and several other coding experiences.
- ⚡ Fun fact: I like to snowboard and ice skate.
- 📫 Reach me by my email (on my profile location) or [LinkedIn](https://www.linkedin.com/in/bogucki-nicholas/).
- 📋 There's a lot of code I should upload to GitHub. Maybe one day...
- 🔭 I've created css userstyles in the past such as a [YouTube anti-distraction](https://userstyles.world/style/1651/youtube-anti-distraction) style. Other styles [here](https://userstyles.org/users/639166). Note this requires something to inject user styles. I personally recommend the [Stylus](https://github.com/openstyles/stylus#releases) extension.

---

[![FrostyNick's GitHub stats](https://github-readme-stats.vercel.app/api?username=frostynick&count_private=true&theme=transparent&show_icons=true&hide=stars&hide_border=true)](https://github.com/anuraghazra/github-readme-stats)
<!-- [![FrostyNick's GitHub stats](https://github-readme-stats.vercel.app/api?username=frostynick&count_private=true&theme=transparent&show_icons=true&hide=stars#gh-light-mode-only)](https://github.com/anuraghazra/github-readme-stats#gh-dark-mode-only) -->

---

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=frostynick&layout=compact&theme=transparent&langs_count=6&hide_border=true)](https://github.com/anuraghazra/github-readme-stats)

<!--
**FrostyNick/FrostyNick** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
